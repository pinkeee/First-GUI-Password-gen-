# ------------- first ever GUI and password gen! -------------
import random
from tkinter import *
from tkinter.ttk import *

def low():
    output.delete(0, END)
    length = var1.get()

    lower = "abcdefghijklmnopqrstuvwxyz"
    caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    spec = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&*"
    password = ""

    if var.get() == 1:
        for i in range(0, length):
            password = password + random.choice(lower)
        return password

    elif var.get() == 0:
        for i in range(0, length):
            password = password + random.choice(caps)
        return password

    if var.get() == 3:
        for i in range(0, length):
            password = password + random.choice(spec)
        return password
    else:
        print("Please choose an option")

def gen(*args):
    password1 = low()
    output.insert(0, password1)

def exit1():
    exit()

ui = Tk()
ui.geometry("600x400")
var = IntVar()
var1 = IntVar()
ui.title("Password Generator")

path = "random.gif"

#Creates a Tkinter-compatible photo image, which can be used everywhere Tkinter expects an image object.
img = PhotoImage(file=path)

#The Label widget is a standard Tkinter widget used to display a text or image on the screen.
panel = Label(ui, image = img)
panel.pack(side = "bottom", fill = "both", expand = "yes"
panel.place(x=255, y=140)

middle = Radiobutton(ui, text="Medium", variable=var, value=0)
middle.place(x=255, y=120)

strong = Radiobutton(ui, text="Strong", variable=var, value=3)
strong.place(x=340, y=120)

output = Entry(ui, font=(30))
output.place(x=200, y=320)

gen_button = Button(ui, text="Generate", command=gen)
gen_button.place(x=257, y=250)

radio_low = Radiobutton(ui, text="Low", variable=var, value=1)
radio_low.place(x=190, y=120)

passwordd = Combobox(ui, text=var1)
passwordd['values'] = (7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24)
passwordd.place(x= 210, y=45)

combo_l = Label(ui, text="Please select your password length.", font=(16))
combo_l.place(x=160, y=10)

exit_b = Button(ui, text=("Exit"), command=exit1)
exit_b.place(x= 510, y=360)

ui.mainloop()
